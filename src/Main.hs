{-# Language
  TypeFamilies,
  DataKinds,
  KindSignatures,
  MultiParamTypeClasses,
  TypeApplications,
  ScopedTypeVariables,
  AllowAmbiguousTypes,
  FlexibleInstances,
  UndecidableInstances,
  GADTs,
  TypeOperators,
  PolyKinds,
  FunctionalDependencies,
  FlexibleContexts
  #-}

module Main where

import Control.Applicative

data Proof
  = Apply Proof Proof
  | Assume Nat

data Nat
  = Succ Nat
  | Zero

-- This type family is closed so that it can have a polymorphic kind
type family Concat a b where
  Concat '[] bs = bs
  Concat (a ': as) bs = a ': Concat as bs

-- This type family is closed so that it can have a polymorphic kind
type family SndCons2 a b where
  SndCons2 b '(c, d) = '(c, b ': d)

-- This type family is closed so that it can have a polymorphic kind
type family SndCons3 a b where
  SndCons3 b '(c, d, a) = '(c, b ': d, a)

-- This type family is closed so that it can have a polymorphic kind
type family FstCons a b where
  FstCons b '(c, d, a) = '(b ': c, d, a)

class If
  (a :: Bool) b c d
  | a b c -> d
instance If 'True  a b a
instance If 'False a b b

type family
  NumberOfArgs
    ( a :: * )
      :: Nat
  where
    NumberOfArgs (a -> b) = 'Succ (NumberOfArgs b)
    NumberOfArgs a = 'Zero

type family
  ProcessHead
    (used :: Bool)
    (head :: (*, Proof))
    (candidates :: [(*, Proof)]) 
      :: ([(*, Proof)], [(*, Proof)], Bool)
  where
    ProcessHead used head '[] =
      '( '[]
      ,  '[]
      ,  used
      )
    -- If our head is a function and we find it's input we can create its output
    -- Only if it has not been consumed prior
    -- We will consume the head since functions can't be called twice, but ...
    -- We will continue to process since our head might be the input to other functions
    ProcessHead 'False '((a -> b), p1) ( '(a, p2) ': c) =
      FstCons '(b, 'Apply p2 p1)
       ( SndCons3 '(a, p2)
         ( ProcessHead 'True '((a -> b), p1) c
         )
       )
    ProcessHead used '(a, p1) ( '((a -> b), p2) ': c) =
      FstCons '(b, 'Apply p1 p2)
        ( ProcessHead used '(a, p1) c
        )
    -- If none of the previous was meet
    -- move the front of the list to the non-consumed list and process the rest
    ProcessHead used a (b ': c) =
      SndCons3 b
        ( ProcessHead used a c
        )

class NextRow
  ( inSpace :: ([(*, Proof)], [(*, Proof)]) )
  ( outSpace :: ([(*, Proof)], [(*, Proof)]) )
  | inSpace -> outSpace
instance NextRow '( '[], passiveTargets) '( '[], passiveTargets)
instance
  ( ProcessHead 'False head activeTargets ~ '(generated1, remaining1, used1)
  , ProcessHead used1 head passiveTargets ~ '(generated2, remaining2, used2)
  , NextRow '(remaining1, remaining2) '(allGenerated, allRemaining)
  , Concat generated1 (Concat generated2 allGenerated) ~ totalGenerated
  , If used2 allRemaining (head ': allRemaining) totalRemaining
  )
    => NextRow
      '(head ': activeTargets
       , passiveTargets
       )
      '( totalGenerated
       , totalRemaining 
       )

class Indexed (n :: Nat) (a :: [*]) (b :: [(*, Proof)]) | a n -> b
instance Indexed n '[] '[]
instance
  ( Indexed ('Succ n) b c
  )
    => Indexed n (a ': b) ('(a, 'Assume n) ': c)

class FindProof
  ( goal :: * )
  ( args :: [*] )
  ( proof :: Proof )
  | goal args -> proof
instance
  ( args ~ (genHead ': genTail)
  , Indexed 'Zero args indexedArgs
  , Resolve goal '( indexedArgs, '[] ) (MatchToProof goal indexedArgs) proof
  )
    => FindProof goal args proof

class Resolve
  ( goal :: * )
  ( resultSpace :: ([(*, Proof)], [(*, Proof)]) )
  ( match :: Maybe (Proof) )
  ( proof  :: Proof )
  | goal resultSpace match -> proof
-- If a proof was found use it
instance Resolve g p ('Just proof) proof
-- If a proof was not found iterate again
instance
  ( NextRow resultSpace '(newActiveTargets, newPassiveTargets)
  -- Active targets must be non-empty
  , newActiveTargets ~ (dummy1 ': dummy2)
  , Resolve goal '(newActiveTargets, newPassiveTargets) (MatchToProof goal newActiveTargets) proof
  )
    => Resolve goal resultSpace 'Nothing proof

-- Could be polymorphic
type family
  MatchToProof
    ( goal :: *)
    ( searchSpace :: [(*, Proof)])
      :: Maybe Proof
  where
    MatchToProof goal '[] =
      'Nothing
    MatchToProof goal ('(goal, proof) ': searchSpace) =
      'Just proof
    MatchToProof goal ('(notGoal, proof) ': searchSpace) =
      MatchToProof goal searchSpace

data HList (a :: [ * ]) where
  HNil :: HList '[]
  (:>) :: a -> HList b -> HList (a ': b)

hHead :: HList (a ': b) -> a
hHead (a :> _) = a

hTail :: HList (a ': b) -> HList b
hTail (_ :> b) = b

class Construct (a :: Proof) (b :: [*]) c | a b -> c where
  construction :: HList b -> c
instance Construct ('Assume 'Zero) (a ': b) a where
  construction = hHead
instance
  ( Construct ('Assume n) b goal
  )
    => Construct ('Assume ('Succ n)) (a ': b) goal
  where
    construction = construction @('Assume n) . hTail
instance
  ( Construct argProof   args argGoal
  , Construct functProof args functGoal
  -- Our function goal is just a function from our argGoal to our mainGoal
  , (argGoal -> mainGoal) ~ functGoal
  )
    => Construct ('Apply argProof functProof) args mainGoal
  where
    construction args =
      ($)
        (construction @functProof args)
        (construction @argProof   args)

type family
  IsAtomic
    ( a :: * )
      :: Bool
  where
    IsAtomic (a -> b) = 'False
    IsAtomic a = 'True

class
  SeparateArgs args goal newSig
  | args goal -> newSig
  , newSig -> args goal
  where
    refunct :: (HList args -> goal) -> newSig

instance
  ( IsAtomic newSig ~ isAtomic -- For (possible? compilation time) speedup
  , SeparateArgs' isAtomic args goal newSig
  )
    => SeparateArgs
      (args :: [*])
      (goal :: *)
      (newSig :: *)
  where
    refunct = refunct' @isAtomic

class
  SeparateArgs'
    (isAtomic :: Bool)
    (args :: [*])
    (goal :: *)
    (newSig :: *)
  | args goal -> newSig isAtomic
  , newSig isAtomic -> args goal
  where
    refunct' :: (HList args -> goal) -> newSig
instance
  ( IsAtomic goal ~ 'True -- Only exists to ensure we are not invoking this in an illegal manner
  )
    => SeparateArgs' 'True '[] goal goal
  where
    refunct' makeA = makeA HNil
instance
  ( IsAtomic (headArg -> c) ~ 'False -- Only exists to ensure we are not invoking this in an illegal manner
  , SeparateArgs tailArgs goal c
  )
    => SeparateArgs' 'False (headArg ': tailArgs) goal (headArg -> c)
  where
    refunct' hFunct x = refunct $ hFunct . (x :>)

type family MapFst args where
  MapFst '[] = '[]
  MapFst ('(a, b) ': c) = a ': MapFst c

class Functive a where
  function :: a

instance
  ( args ~ (genHead ': genTail)
  , FindProof goal args proof
  , Construct proof args goal
  , SeparateArgs args goal a
  )
    => Functive a
  where
    function =
      refunct @args $ construction @proof 

main=return ()

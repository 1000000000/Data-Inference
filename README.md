# Data Inference

Many languages have type inference.  That is given some data the compiler can determine its type.

However can we do the opposite?  Given a type determine it's data?

There are already some types in Haskell that are unambiguous for example if

    f :: forall a . a -> a

Then there is only one total function (not `undefined`) that `f` can be

    f = id

or 

    f x = x

We can even deduce data from more complex types for example

    f :: forall a. forall b. forall c. (a -> b -> c) -> (b -> a -> c)

can only be `flip`! (so long as `f` is total)


---

The goal of this repository is to build the theory around data inference from types in Haskell's type system and some of its extensions in GHC, but also to build some concrete implementations of the algorithms.

# `function`

Currently most of the work has been focused on building something called `function`.

`function` is a polymorphic function that will change it's behavior to match the type to which it is constrained.

Here are some examples of function being envoked in ghci:

    *Main> (function "Hi" 2 () True) :: Integer
    2
    *Main> (function "Hi" 2 () True) :: String
    "Hi"
    *Main> (function 9 (> 9)) :: Bool
    False
    *Main> (function ["Hello", "World"] unlines putStr) :: IO ()
    Hello
    World

It can even be called with new data types it has never seen before

    *Main> data Tree = Elm | Oak | Sycamore | Pine | Birch | Cedar deriving Show
    *Main> (function 1 Elm "Oak") :: Tree
    Elm
    *Main> getTree x | x = Pine | not x = Cedar
    *Main> (function 1 getTree (> 3)) :: Tree
    Cedar
    *Main>

If you would like to play around with `function` you can test it out by running `stack ghci` and envoking it there.

It should be noted that currently `function` does not play nicely with any sort of polymorphism.
Polymorphic arguments or results will almost always cause a type error.
You can often get around this by constraining `function` before calling it to eliminate input polymorphism.  For example:

    *Main> (function [1..10] map (>8)) :: [Bool]

Will produce dozens of lines of error messages while

    *Main> (function :: [Integer] -> ((Integer -> Bool) -> [Integer] -> [Bool]) -> (Integer -> Bool) -> [Bool]) [1..10] map (>8)
    [False,False,False,False,False,False,False,False,True,True]

works properly.

The feasibility of polymorphic input is being investigated.

# Related Reading

 - [*Theorems for Free!*, Philip Wadler](https://ecee.colorado.edu/ecen5533/fall11/reading/free.pdf)
